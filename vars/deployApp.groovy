#!/usr/bin/env groovy

def call() {
    echo 'Deploying deploying docker image to EC2...'

    def shellCmd = "bash ./server-cmds.sh ${IMAGE_NAME}"
    def ec2Instance = "ec2-user@3.126.51.66"

    sshagent(['ec2-server-key']) {
        sh "scp server-cmds.sh ${ec2Instance}:/home/ec2-user"
        sh "scp docker-compose.yml ${ec2Instance}:/home/ec2-user"
        sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCmd}"
    }
}
